#include <queue>
#include <thread>
#include <mutex>
#include <memory>
#include <condition_variable>

using namespace std;

//Thread safe queue implementation built over std::queue.
template <typename T>
class Queue
{
 public:
  
  //pop function to output the top item of the queue, in this case a branch 
  T pop()
  {
    unique_lock<mutex> lock(m); //locks the structure
    while (data.empty()){ //if the queue is empty then need to wait
      cv.wait(lock);
    }
    auto item = data.front(); //gets the item from the top of the queue
    data.pop(); //pops the item off
    return item; //and then returns the item
  }
  
  //similar to above but the type of the popped item is getting called but doesn't return anything,
  //just a queue operation
  void pop(T &item)
  {
    unique_lock<mutex> lock(m);
    while (data.empty()){
      cv.wait(lock);
    }
    item = data.front();
    data.pop();
  }
  
  //pushes a new item onto the queue
  void push(const T &item)
  {
    lock_guard<mutex> lock(m); //need to lock the stucture
    data.push(item); //then push the item so multiple aren't pushing same time
    cv.notify_one(); //notifies a waiting thread, so that can then push
  }
  
  //similar to above but takes in the reference item and adds it to the queue
  void push(T &&item)
  {
    lock_guard<mutex> lock(m);
    data.push(move(item)); //needs moving into the topn of the queue
    cv.notify_one();
  }
 
 private:
  queue<T> data; //the std:queue of type T
  mutex m;  //define the mutex
  condition_variable cv;  //and condition variable
};