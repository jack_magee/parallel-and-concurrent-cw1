#include <iostream>
#include <stdio.h>
#include "tgmath.h" //for using pow()
#include "Branch.h" //my class for storing branches
#include "Queue.h"  //threadsafe queue for storing data
#include "thread"
#include <vector>
#include "atomic"
#include <sys/time.h> //for wall clock time

using namespace std;
typedef int chessboard;
atomic<int> solutions; //define an atomic int so to deal with multiple threads accessing at the same time
Queue<Branch> *list = new Queue<Branch>; //define the global queue of branches so all threads and function calls can access

//Provided code for the coursework.
int seq_nqueen(chessboard ld, chessboard cols, chessboard rd, const chessboard all){
  int sol = 0;	
  if (cols == all)                            // A solution is found 
    return 1; 

  chessboard pos = ~(ld | cols | rd) & all;  // Possible posstions for the queen on the current row
  
  chessboard next; 
  while (pos != 0){                          // Iterate over all possible positions and solve the (N-1)-queen in each case
    next = pos  & (-pos);                    // next possible position 
    pos -= next;                             // update the possible position 
    sol += seq_nqueen((ld|next) << 1, cols|next, (rd|next)>>1, all); // recursive call for the `next' position 	   
  }
  return sol; 
}

//Parallel function for nqueens, similar to one above but takes a start location and end location
void par_nqueen(chessboard ld, chessboard cols, chessboard rd, const chessboard all){
  Branch curBran =  list->pop(); //find the next branch on the queue
  chessboard from = curBran.from(); //get the location to search from
  chessboard to = curBran.to(); //and the location to search to
  int sol = 0;	
  if (cols == all){                          // A solution is found 
      solutions++;
  }
  chessboard pos = from;  // Possible posstions for the queen on the current row
  chessboard next; 
  while (pos != 0){                          // Iterate over all possible positions and solve the (N-1)-queen in each case
    next = pos  & (-pos);                    // next possible position 
    if(pos == to){
        break;
    }
    pos -= next;                             // update the possible position 
    sol += seq_nqueen((ld|next) << 1, cols|next, (rd|next)>>1, all); // recursive call for the `next' position down the given branch 	   
  }
  solutions += sol; //update the atomic value of solutions
}

//function to make the queue of all the top level branches
//This will always be a finite value of size n.
void makeQueue(int size, Queue<Branch> *list)
{   int a = 0;
    int b = -1;
    int val = 0;
    int power = pow(2,size)-1; //init size of board to all 1's
    for (int i=size+1;i > 0; i--){ //work backwards to find locations
        if (b == -1) {
            a = power-val; //location to search to
            b = a; //from the previous value
        }
        else {
            b = a;
            a = power - val;
            Branch bran = Branch(b,a); //generate each branch
            list->push(bran);//add the chessboards to the queue
        }
        val += pow(2,(size + 1) -i); //move the value along to find the next branch
    }
}

//function to generate a thread for each of the limited branches in the queue.
//There will be a total of n threads that will work on the data
template<typename T>
void initThread(vector<thread> &threads, T &&func)
{
    //takes in the vector of thread objects and the funtion to be ran on each thread
    for(auto &&trd: threads){// find a non used thread
        if(trd.joinable()){     //if the thread is joinable then its not doing anything so can be put to use
            continue;
        }
        trd = thread(func); //set the threaad to run the given function
        return;
    }
}

int main(int argc, char** argv)
{
    int n= stoi(argv[1]); //take the nunmber of queens
    struct timeval start, end; //used for wall clock timeing the code
    gettimeofday(&start, NULL);
    thread t1(makeQueue ,n, list); //make a thread generate the queue of the top level branches
    t1.join();
    unsigned int nthreads = n; //the total number of times the functions will be called on independent threads
    vector<thread> threads(nthreads); //create vector of thread objects
    solutions = 0;
    for(int i  = 0; i < n ; i++){ //for all the columns on the chessboard for each queen
        //init the threads to be calling the parallel function using lambda calls
        initThread(threads, [=]{par_nqueen(0,0,0, (1 << n) - 1);});
    }
    //checks to see if the threads have been used up and finished, if so then join
    for(auto &&trd: threads){
        if(trd.joinable()){
            trd.join();
        }
    }
        
    gettimeofday(&end, NULL);
    long seconds = ((end.tv_sec - start.tv_sec)); //calc time taken
    long time_taken = ((seconds*1000000) + end.tv_usec) - (start.tv_usec);
    cout << "Time taken: " << time_taken<< "seconds" << "\n";
    cout << solutions<<"\n";
    return 0;
}