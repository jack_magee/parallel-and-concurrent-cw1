#include <stdlib.h>
#include <iostream>

typedef int chessboard;

//My class for storing the start and end of each branch to add to the queue.
//It takes the board position for the start board, and a board position
//for the final position.
class Branch {
    public:
    Branch() {}
    Branch(chessboard e0, chessboard e1) {e[0] = e0; e[1] = e1;}
    inline chessboard from() const {return e[0];} //start board for each branch
    inline chessboard to() const {return e[1];} //end board for each branch

    private:
    chessboard e[2];
};

//print function used for testing
inline std::ostream& operator<<(std::ostream &os, const Branch &t) {
	os << t.from() << " " << t.to();
	return os;
}